<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Homecontroller@home');
Route::get('/register', 'AutController@form');
Route::POST('/welcome', 'AutController@welcome');

Route::get('/data-table', function(){
    return view('page1.data-table');
});
Route::get('/table', function(){
    return view('page1.table');
});