@extends('layout.master')
@section('title')
    Halaman Form   
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="fname"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="lname"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="GR">Male <br>
        <input type="radio" name="GR">Female <br>
        <input type="radio" name="GR">Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="Negara">
            <option value="Indonesia">Indonesia</option>
            <option value="Inggris"> Inggris</option>
            <option value="Other"> Other</option>
        </select> <br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox" name="Bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="Bahasa">English <br>
        <input type="checkbox" name="Bahasa">Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Sign Up">

    </form> 
@endsection
   