<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AutController extends Controller
{
    public function form()
    {
        return view('page1.form');
    }
    public function welcome(Request $request)
    {
        //dd($request->all());
        $first = $request['fname'];
        $last = $request['lname'];

        return view('page1.welcome', compact('first','last'));
    }
}
